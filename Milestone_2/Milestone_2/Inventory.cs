﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Milestone_2
{
    public class Inventory
    {
        private String ProductName;
        private String ProductDesc;
        private Double Price;
        private Boolean IsStocked;
        private Int32 Quantity;

        public void Home()
        {
            //Directs user to homepage
        }
        public void ViewCurrent()
        {
            //Directs user to current inventory
        }
        public void OrderNew()
        {
            //Directs user to GUI that takes order requests
        }
        public void CancelOrder()
        {
            //Directs user to GUI of list of pending orders to be cancelled
        }
        public void RemoveItem()
        {
            //Directs user to GUI of current inventory so that user may remove items off inventory
        }
        public void Edit()
        {
            //Allows user to edit items in inventory
        }
    }
}
