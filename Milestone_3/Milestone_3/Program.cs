﻿using Milestone_3;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Milestone_2
{
    class Program
    {
        static void Main(string[] args)
        {
            
        }

        void TestClass()
        {
            Inventory test = new Inventory();
            ArrayList inventory = new ArrayList();
            test.OrderNew("Pencil", 2, inventory);
            test.RemoveItem("Pencil", inventory);
            test.OrderNew("Pen", 5, inventory);
            test.ViewCurrent(inventory);
            test.Search("Pen", 2, inventory);
        }
    }
}
