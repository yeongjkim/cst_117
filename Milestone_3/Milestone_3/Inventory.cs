﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Milestone_3
{
    public class Inventory
    {
        private String ProductName;
        private String ProductDesc;
        private Double Price;
        private Boolean IsStocked;
        private Int32 Quantity;
        private String Item;

        public void Home()
        {
            //Directs user to homepage
        }
        public void ViewCurrent(ArrayList inventory)
        {
            //Directs user to current inventory
            foreach (int i in inventory)
            {
                Console.WriteLine();
            }
        }
        public void OrderNew(String item, int quantity, ArrayList inventory)
        {
            //Directs user to GUI that takes order requests
            for (int i = 0; i < quantity; i++)
            {
                inventory.Add(item);
            }
        }
        public void CancelOrder()
        {
            //Directs user to GUI of list of pending orders to be cancelled
        }
        public void RemoveItem(String item, ArrayList inventory)
        {
            //Directs user to GUI of current inventory so that user may remove items off inventory
            inventory.Remove(item);
        }
        public void Edit()
        {
            //Allows user to edit items in inventory
        }
        public void Search(string name, int quantity, ArrayList inventory)
        {
            //Finds item in inventory and displays quantity
            int n = 0;
            int q = 0;
            foreach (int i in inventory)
            {
                if (inventory.Contains(name))
                    n++;
            }
            if (n == 0)
                Console.WriteLine("Item does not exist");
            else
                Console.WriteLine(name + " has a quantity of " + n);
        }

    }
}
