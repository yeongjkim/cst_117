﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Exercise_10
{
    class Program
    {
        static void Main(string[] args)
        {

            int count = 0;

            string text = File.ReadAllText("Text.txt");
            Console.WriteLine(text);

            string[] tokens = text.Split(null);

            foreach (string s in tokens)
            {
                String mod = s.Replace("@", "").Replace(",", "").Replace(".", "").Replace(";", "").Replace("'", "").Replace("!", "");
                bool endsWithE = mod.EndsWith("e", System.StringComparison.CurrentCultureIgnoreCase);
                bool endsWithT = mod.EndsWith("t", System.StringComparison.CurrentCultureIgnoreCase);

                if (endsWithE || endsWithT)
                    count++;
            }
            Console.WriteLine("There are " + count + " words that end in e or t.");

            Console.Read();
        }
    }
}
