﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise_7
{
    class Program
    {
        static void Main(string[] args)
        {
        }

        //1
        public void sum(int a, int b) {
            Console.Write(a + b);
        }

        //2
        public double avg (double a, double b, double c, double d, double e) {
            return (a + b + c + d + e) / 5;
        }

        //3
        public Int32 randomSum(int a, int b) {
            Random rand = new Random();

            a = rand.Next();
            b = rand.Next();

            return a + b;
        }

        //4
        public Boolean divisByThree (int a, int b, int c)
        {
            if ((a + b + c) % 3 == 0)
                return true;
            else
                return false;
        }

        //5
        public void longerString (string a, string b)
        {
            if (a.Length > b.Length)
                Console.WriteLine(a);
            else
                Console.WriteLine(b);
        }

        //6
        public Double largestDub(double[] dubs)
        {
            double large = 0;
            for (int i = 1; i < dubs.Length+1; i++)
            {
                if (dubs[i] > large)
                    large = dubs[i];
            }
            return large;
        }

        //7
        public Int32 fiftyInts(int[] fitty)
        {
            return fitty[50];
        }

        //8
        public bool same(bool a, bool b)
        {
            if (a == b)
                return true;
            else
                return false;
        }

        //9 
        public Double intDoub (int a, double b)
        {
            return a + b;
        }

        //10?????????????????????????????????????????????????????????????????????????????????????????????????????
        public Int32 avgOfArray(Int32[][] twoArray)
        {
            return 1;
        }

    }
}
