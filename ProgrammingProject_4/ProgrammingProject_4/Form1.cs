﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProgrammingProject_4
{
    public partial class Form1 : Form
    {
        bool turn = true;
        int turnCount = 0;

        public Form1()
        {
            InitializeComponent();
        }

        private void Exit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void button_click(object sender, EventArgs e)
        {
            Button b = (Button)sender;
            if (turn)
                b.Text = "X";
            else
                b.Text = "O";

            turn = !turn;
            b.Enabled = false;

            turnCount++;

            checkForWinner();
        }

        private void checkForWinner()
        {
            bool theresWinner = false;

            string[,] array = new string[,]
            {
                {A1.Text, A2.Text, A3.Text},
                {B1.Text, B2.Text, B3.Text},
                {C1.Text, C2.Text, C3.Text}
            };

            if (array[0, 0] == array[0, 1] && array[0, 1] == array[0, 2] && (!A1.Enabled))
                theresWinner = true;
            else if (array[1, 0] == array[1, 1] && array[1, 1] == array[1, 2] && (!B1.Enabled))
                theresWinner = true;
            else if (array[2, 0] == array[2, 1] && array[2, 1] == array[2, 2] && (!C1.Enabled))
                theresWinner = true;

            if (array[0, 0] == array[1, 0] && array[1, 0] == array[2, 0] && (!A1.Enabled))
                theresWinner = true;
            else if (array[0, 1] == array[1, 1] && array[1, 1] == array[2, 1] && (!A2.Enabled))
                theresWinner = true;
            else if (array[0, 2] == array[1, 2] && array[1, 2] == array[2, 2] && (!A3.Enabled))
                theresWinner = true;

            if (array[0, 0] == array[1, 1] && array[1, 1] == array[2, 2] && (!A1.Enabled))
                theresWinner = true;
            else if (array[2, 0] == array[1, 1] && array[1, 1] == array[0, 2] && (!C1.Enabled))
                theresWinner = true;


            if (theresWinner)
            {
                disableButtons();

                String winner = "";
                if (turn)
                    winner = "O";
                else
                    winner = "X";

                MessageBox.Show(winner + " is the winner!");
            }
            else
            {
                if (turnCount == 9)
                    MessageBox.Show("Draw!");
            }
        }

        private void disableButtons()
        {
            try
            {
                foreach (Control c in Controls)
                {
                    Button b = (Button)c;
                    b.Enabled = false;
                }
            }
            catch { }
        }

        private void NewGame_Click(object sender, EventArgs e)
        {
            turn = true;
            turnCount = 0;
            try
            {
                foreach (Control c in Controls)
                {
                    Button b = (Button)c;
                    b.Enabled = true;
                    b.Text = "";
                }
            }
            catch { }
        }
    }
}
