﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise_9
{
    class Set
    {
        private List<int> elements;


        public Set()
        {
            elements = new List<int>();
        }

        public bool addElement(int val)
        {
            if (containsElement(val))
                return false;
            else
            {
                elements.Add(val);
                return true;
            }
        }

        private bool containsElement(int val)
        {
            for (int i = 0; i < elements.Count; i++)
            {
                if (val == elements[i])
                    return true;
                //Doesn't check other values of array. Void code
                //else
                //   return false;
            }
            return false;
        }

        public override string ToString()
        {
            string str = "";
            foreach (int i in elements)
            {
                str += i + " ";
            }
            return str;
        }

        public void clearSet()
        {
            elements.Clear();
        }

        public Set union(Set rhs)
        {
            for (int i = 0; i < this.elements.Count; i++)
            {
                //This is add all elements. Unions do not include duplicates
                //this.addElement(rhs.elements[i]);
                if (!rhs.elements.Contains(this.elements[i]))
                {
                    //If we are trying to return rhs we need to add elements to rhs not this.elements
                    //this.addElement(rhs.elements[i]);
                    rhs.addElement(this.elements[i]);
                }
            }
            return rhs;
        }

    }
}
