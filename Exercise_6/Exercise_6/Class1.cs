﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise_6
{
    class Dice
    {
        private int numOfSides1;
        private int numOfSides2;


        public int RollDies()
        {
            Random rand = new Random();

            numOfSides1 = rand.Next(4, 21);
            numOfSides2 = rand.Next(4, 21);

            return (rand.Next(1, numOfSides1+1)) + (rand.Next(1, numOfSides2+1));
        }

    }
}
