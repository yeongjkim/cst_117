﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise_6
{
    class Program
    {
        static void Main(string[] args)
        {
            int total = 0;
            int tries = 0;
            int cont;

            Dice d = new Dice();
            
            while (total != 2)
            {
                Console.WriteLine("Enter the '1' key to roll dice");
                cont = Convert.ToInt32(Console.ReadLine());
                if (cont == 1)
                {
                    total = d.RollDies();
                    Console.WriteLine("You have rolled a " + total + "\n");
                    tries++;
                }
                else
                    break;

      
            }
            Console.WriteLine("It took " + tries + " rolls to get snake eyes!");

            Console.Read();
        }
    }
}
