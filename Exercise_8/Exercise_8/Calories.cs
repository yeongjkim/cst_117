﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise_8
{
    public class Calories
    {

        public Int32 FatCalories (int fat)
        {
            return fat * 9;
        }

        public Int32 CarbCalories (int carb)
        {
            return carb * 4;
        }

    }
}
