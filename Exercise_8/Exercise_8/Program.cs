﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*
Calories from Fat and Carbohydrates

A nutritionist who works for a fitness club helps members by evaluating their diets.
As part of her evaluation, she asks members for the number of fat grams and
carbohydrate grams that they consume in a day.Then, she calculates the number of
calories that result from the fat using the following formula:

Calories from fat = Fat grams × 9

Next, she calculates the number of calories that result from the carbohydrates using
the following formula:

Calories from carbs = Carbs grams × 4

Create an application that will make these calculations.In the application, you
should have the following methods:

    - FatCalories– This method should accept a number of fat grams as an argument
    and return the number of calories from that amount of fat.
    - CarbCalories– This method should accept a number of carbohydrate
    grams as an argument and return the number of calories from that amount
    of carbohydrates.
*/


namespace Exercise_8
{
    class Program
    {
        static void Main(string[] args)
        {
            Calories cals = new Calories();

            Console.WriteLine("How many grams of fat do you consume in average on a daily basis?");
            int fats = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Calories from fats: " + cals.FatCalories(fats));

            Console.WriteLine("How many grams of carbohydrates do you consume in average on a daily basis?");
            int carbs = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Calories from carbs: " + cals.CarbCalories(carbs));
        }
    }
}
